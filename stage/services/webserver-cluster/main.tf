provider "aws" {
    region = "us-east-2"
}

terraform {
  backend "s3" {
    key = "stage/services/webserver-cluster/terraform.tfstate"
  }
}

module "webserver_cluster" {
  source = "../../../modules/services/webserver-cluster"

  cluster_name = var.cluster_name
  db_remote_state_bucket = "damian-s3-bucket"
  db_remote_state_key = "stage/data-stores/mysql/terraform.tfstate" 
}

