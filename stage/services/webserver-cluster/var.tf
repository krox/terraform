variable "cluster_name" {
  description = "The name to use to namespace all the resources in the stage cluster"
  type = string
  default = "webservers-stage"
}
/*
variable "db_remote_state_bucket" {
  description = "The name of the S3 bucket for the stage database's remote state storage"
  type = string
}

variable "db_remote_state_key" {
  description = "The path for the stage database's remote state in S3"
  type = string
}
*/
