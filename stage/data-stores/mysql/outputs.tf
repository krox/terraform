output "address" {
  value       = module.database.address
  description = "connect to the stage database"
}

output "port" {
  value       = module.database.port
  description = "The port that the stage database is listening on"
}