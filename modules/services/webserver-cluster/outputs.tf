output "alb_dns_name" {
  value = aws_lb.example.dns_name
  description = "The domain name of the load balancer"
}

output "asg_name" {
  value = aws_autoscaling_group.example.name
  description = "The name of the Auto Scaling Group"
}

#output "instance_name" {
#	value				= aws_instance.example.tags["Name"]
#}

#output "public_ip" {
#	value				= aws_instance.example.public_ip
#}

