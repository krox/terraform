variable "db_password" {
  description = "the password for mysql database"
  type        = string
}

variable "db_name" {
  description = "The name of the database"
  type = string
}

