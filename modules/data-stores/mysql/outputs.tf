output "address" {
  value       = aws_db_instance.example.address
  description = "connect to the database"
}

output "port" {
  value       = aws_db_instance.example.port
  description = "The port that the database is listening on"
}