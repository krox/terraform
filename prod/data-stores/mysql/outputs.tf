output "address" {
  value       = module.database.address
  description = "connect to the prod database"
}

output "port" {
  value       = module.database.port
  description = "The port that the prod database is listening on"
}