provider "aws" {
  region = "eu-west-2"
}

terraform {
  backend "s3" {
    key = "prod/data-stores/mysql/terraform.tfstate"
  }
}

module "database" {
  source = "../../../modules/data-stores/mysql"

  db_name = var.db_name
  db_password = var.db_password
}























