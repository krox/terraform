provider "aws" {
    region = "us-east-2"
}

terraform {
  backend "s3" {
    key = "prod/services/webserver-cluster/terraform.tfstate"
  }
}

module "webserver_cluster" {
  source = "../../../modules/services/webserver-cluster"
  
  cluster_name = var.cluster_name
  db_remote_state_bucket = "damian-s3-bucket"
  db_remote_state_key = "prod/data-stores/mysql/terraform.tfstate"
}
/*
resource "aws_autoscaling_schedule" "scale_out_during_business_hours" {
  autoscaling_group_name = module.webserver_cluster.asg_name
  scheduled_action_name = "scale_out_during_business_hours"
  min_size = 3
  max_size = 4
  desired_capacity = 3
  recurrence = "0 9 * * *"

}

resource "aws_autoscaling_schedule" "scale_in_at_night" {
  autoscaling_group_name = module.webserver_cluster.asg_name
  scheduled_action_name = "scale_in_at_night"
  min_size = 2
  max_size = 3
  desired_capacity = 2
  recurrence = "0 17 * * *"
}
*/
